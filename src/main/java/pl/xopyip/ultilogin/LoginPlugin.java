package pl.xopyip.ultilogin;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import pl.xopyip.ultilogin.map.Map;

import java.lang.reflect.Field;

/**
 * Created by xopyi on 2015-08-16.
 */
public class LoginPlugin extends JavaPlugin {
    private SimpleCommandMap commandMap;
    private static LoginPlugin instance;

    public static LoginPlugin getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        try {
            Field f = getServer().getClass().getDeclaredField("commandMap");
            f.setAccessible(true);
            commandMap = (SimpleCommandMap) f.get(getServer());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        commandMap.register("ultilogin", new Command("login") {
            @Override
            public boolean execute(CommandSender commandSender, String s, String[] strings) {
                if(commandSender instanceof Player)new Map((Player)commandSender, "hehehe");
                return true;
            }
        });
        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onJoin(PlayerJoinEvent event){
                new Map(event.getPlayer(), "hehehes");
            }
        }, this);
    }
}
