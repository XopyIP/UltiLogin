package pl.xopyip.ultilogin.map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapView;
import pl.xopyip.ultilogin.LoginPlugin;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * Created by xopyi on 2015-08-16.
 */
public class Map {
    public Map(Player p, String code) {
        MapView mv = LoginPlugin.getInstance().getServer().createMap(p.getWorld());
        mv.getRenderers().forEach(mv::removeRenderer);
        Renderer renderer = new Renderer();
        try {
            renderer.setImg(ImageIO.read(getClass().getResourceAsStream("/bg.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setCode(code);
        mv.addRenderer(renderer);
        p.getInventory().setItemInHand(new ItemStack(Material.MAP, 1, mv.getId()));
    }
}
