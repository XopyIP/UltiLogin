package pl.xopyip.ultilogin.map;

import org.bukkit.entity.Player;
import org.bukkit.map.*;

import java.awt.*;

/**
 * Created by xopyi on 2015-08-16.
 */
public class Renderer extends MapRenderer {
    private Image img;
    private String code;

    public void setImg(Image img) {
        this.img = img;
    }

    @Override
    public void render(MapView mapView, MapCanvas mapCanvas, Player player) {
        if(img!=null)
            mapCanvas.drawImage(0, 0, img);
        if(code!=null)
            mapCanvas.drawText((128-MinecraftFont.Font.getWidth(code))/2, 10, MinecraftFont.Font, code);
    }

    public void setCode(String code) {
        this.code = code;
    }
}
